function [ sample_spectr, log_space ] = eject_spectr( sample_length, sample, fs, band_pass )
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here

%% set parameters
band_start = band_pass(1);
band_stop = band_pass(2);
decimation_speed = 2.9;
db_range = 25;
amount_point = 800;

%% create spaces
spectrum_space = 0 : fs/length(sample) : (fs/2) - (1/length(sample));
spectrum_space = spectrum_space(spectrum_space <= band_stop);

index_space = log_index_space(amount_point, length(spectrum_space), decimation_speed);
log_space = create_space(index_space, spectrum_space(1), spectrum_space(end));


%% log decimation
if ((length(sample) / 2) < index_space(end))
    sample_index_space = log_index_space(amount_point, round(length(sample) / 2), decimation_speed);
else
    sample_index_space = index_space;
end
log_sample_space = create_space(sample_index_space, spectrum_space(1), spectrum_space(sample_index_space(end)));


%% create spectral density and interpolation
sample_spectr = 10*log10(abs(fft(sample, length(sample))) ./ (length(sample) * sample_length));
sample_spectr = sample_spectr(1 : length(spectrum_space));

% figure
% subplot(3,1,1)
% plot(spectrum_space, sample_spectr);
% axis tight

sample_spectr = decimation_spectrum( sample_spectr, sample_index_space );

% subplot(3,1,2)
% plot(log_sample_space, sample_spectr);
% axis tight


if ((length(sample) / 2) < index_space(end))
    sample_spectr = interp1(log_sample_space, sample_spectr, log_space, 'linear', 'extrap');
end

% subplot(3,1,3)
% plot(log_space, sample_spectr);
% axis tight
% 
% drawnow
% pause

%% normalization
% spectrum_base = median(sample_spectr(log_space < band_start));
% sample_spectr(log_sample_space(end) < log_space) = spectrum_base;
% sample_spectr(log_space < band_start) = spectrum_base;

sample_spectr = max(sample_spectr, max(sample_spectr) - db_range);


end

