function [ out_voice ] = noise_suppression( in_voice, noise, fs, band_stop )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

spectrum_space = 0 : fs/length(noise) : (fs/2) - (1/length(noise));

%%  compute noise spectrum
spectrum_noise = fft(noise);
spectrum_noise = abs(spectrum_noise(1 : length(spectrum_space)));
spectrum_noise = spectrum_noise ./ max(spectrum_noise); % .* threshold;

%% filter and decimation spectrum noise
% complement_spectrum_noise = 1 - spectrum_noise;
complement_spectrum_noise = log(spectrum_noise);

amount_filter_point = 1000;
spectrum_decimation = round(length(spectrum_noise) / amount_filter_point);

gw = gausswin(spectrum_decimation * 2);
complement_spectrum_noise = filter(gw, 1, complement_spectrum_noise) ./ spectrum_decimation;
complement_spectrum_noise = exp(complement_spectrum_noise(1 : spectrum_decimation : end))';
complement_spectrum_noise(1:2) = mean(complement_spectrum_noise);


% complement_spectrum_noise = 1 - complement_spectrum_noise;
spectrum_gain = (norm(noise) / norm(in_voice)) / mean(complement_spectrum_noise) * 2;
complement_spectrum_noise = complement_spectrum_noise .* spectrum_gain;
complement_spectrum_noise = 1 - complement_spectrum_noise;

complement_spectrum_noise = min(complement_spectrum_noise, 1);
complement_spectrum_noise = max(complement_spectrum_noise, 0);

decimation_spectrum_space = spectrum_space(1 : spectrum_decimation : end);

%%  compute compelement noise filter
order = min(500, amount_filter_point);
b = fir2(order, decimation_spectrum_space ./ decimation_spectrum_space(end), complement_spectrum_noise);

% figure
% plot(decimation_spectrum_space,complement_spectrum_noise, 'b')
% axis tight
% hold on
%
% [h,w] = freqz(b,1,length(complement_spectrum_noise));
% plot(w/pi .* decimation_spectrum_space(end),abs(h), 'r')
% axis tight

%% filtering voice
out_voice = filter(b, 1, in_voice);
[b, a] = butter(6, (band_stop*2) / (fs/2));
out_voice = filter(b, a, out_voice);

out_voice = out_voice ./ max(out_voice);


end

