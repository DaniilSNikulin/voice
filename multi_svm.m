
svm_vector = cell(length(file_names), 1);

for file_iter = 1 : length(file_names)
    theClassInner = ones(360, 1);
    theClassInner(40*(file_iter-1) + 1 : 40*file_iter) = -1;
    theClassInner = -1 .* theClassInner;
    svm_vector{file_iter} = fitcsvm(Component, theClassInner);
end


N = length(file_names)^2;
Scores = zeros(N,length(file_names));
outter_svm_cross_matr = zeros(length(file_names));

for file_iter = 1 : length(file_names)
    
    [x, fs] = audioread(strcat('sample/', outer_file_names{file_iter}));
        
    sec_start = 0;
    sec_stop = floor(length(x) * 5/6 / fs) + 50000000;
    
    frame_stop = min(length(x), sec_stop*fs);
    frame_start = max(sec_start*fs, 1);
    x = mean(x(frame_start : frame_stop, :), 2);
    x = x - mean(x);
    x = outlier_suppression(x, fs);
    
    band_start = 30;
    [b, a] = ellip(3,3,20,band_start * 2 / fs,'high');
    % freqz(b,a,fs,fs)
    
    x = filter(b, a, x);
    x = x ./ max(x);
    

    local_estimation = zeros(9, 1);
    for iter = 1 : 4
        sec_duration = 20;
        last_index = length(x) - sec_duration*fs - 1;
        start_index = randi(last_index);
        stop_index = start_index + sec_duration*fs;
        y = x(start_index : stop_index);
        human = split_to_word(y, fs, 0.1);

        for local_iter = 1 : length(file_names)
            [~, scores] = predict(svm_vector{local_iter}, human);
            local_estimation(local_iter) = local_estimation(local_iter) + scores(2);
        end
    end
    estimation = local_estimation ./ 4
    
    outter_svm_cross_matr(file_iter, :) = estimation;
        
    
end

