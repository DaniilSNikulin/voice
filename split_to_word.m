function [ y ] = split_to_word( x, fs, threshold )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here



%%  compute the power of samples


buffer_duration = 10 * 10^(-3);    % in seconds
overlap = 50;                      % in percent

sample_size = round(buffer_duration * fs);
buffer_size = round( ((1 + 2*(overlap / 100))*buffer_duration) * fs);

% array of power buffer samples
power_array = zeros(floor(length(x) / sample_size), 1);
noise_power_array = zeros(length(power_array), 1);
% spectrum_array = zeros(length(power_array), buffer_size);
for iter = 1 : length(power_array)
    
    start_index = (iter - 1) * sample_size;
    start_index = max(round(start_index - (overlap / 100)*sample_size), 1);
    end_index = min(start_index + buffer_size, length(x));
    
    buffer = x(start_index : end_index);
    power_array(iter) = norm(buffer);
    noise_power_array(iter) = norm(abs(diff(sign(buffer))));
    %     spectrum_array(iter, 1 : length(buffer)) = abs(fft(buffer));
end


%% filtering
gw = gausswin(8);
power_array = filter(gw, 1, power_array);
power_array = power_array - base_value(power_array);
power_array = max(power_array, 0);
power_array = power_array ./ max(power_array);



gw = gausswin(16);
noise_power_array = filter(gw, 1, noise_power_array);
noise_power_array = noise_power_array - median(noise_power_array);
noise_power_array = max(noise_power_array, 0);
noise_power_array = -noise_power_array + max(noise_power_array);
noise_power_array = noise_power_array ./ max(noise_power_array);
noise_power_array = (1 - noise_power_array);


%%  find the end of phrases
phrase_detection = zeros(size(power_array));
max_magn = 0;
for iter = 1 : length(power_array)
    max_magn = max(max_magn, power_array(iter));
    if (power_array(iter) < threshold*max_magn)
        phrase_detection(iter) = max_magn;
        max_magn = 0;
    end
end


%%  find the start of phrases
max_magn = 0;
for iter = length(power_array) : -1 : 1
    max_magn = max(max_magn, power_array(iter));
    if (power_array(iter) < threshold*max_magn)
        phrase_detection(iter) = max_magn;
        max_magn = 0;
    end
end


%% post processing of phrase deection array
phrase_detection = not(phrase_detection);
phrase_detection(1) = 0;
phrase_detection(end) = 0;
edge_phrase = diff(phrase_detection);



%%  create phrase array
phrase_counter = 0;
for iter = 1 : length(edge_phrase)
    if (0 < edge_phrase(iter))
        phrase_counter = phrase_counter + 1;
        phrase(phrase_counter).start = iter;
    elseif (edge_phrase(iter) < 0)
        start = phrase(phrase_counter).start;
        phrase(phrase_counter).end = iter;
        phrase(phrase_counter).power = mean(power_array(start : iter));
        phrase(phrase_counter).noise_power = mean(noise_power_array(start : iter));
    end
end


%%  union small phrases
length_threshold = 16;
iter = 1;
while (iter < length(phrase))
    phrase_length = phrase(iter).end - phrase(iter).start;
    if ((phrase_length < length_threshold) && (iter < length(phrase)) && (iter ~= 1))
        
        lookup_power = phrase(iter+1).power;
        behind_power = phrase(iter-1).power;
        
        if (abs(lookup_power - phrase(iter).power) < abs(behind_power - phrase(iter).power))
            next_iter = iter + 1;
        else
            next_iter = iter - 1;
        end
        power_ratio = phrase(next_iter).power / phrase(iter).power;
        power_ratio = min(power_ratio, 1 / power_ratio);
        if ( power_ratio < threshold )
            iter = iter + 1;
            continue;
        end
        min_index = min(phrase(iter).start, phrase(next_iter).start);
        max_index = max(phrase(iter).end, phrase(next_iter).end);
        
        phrase_next_length = (phrase(next_iter).end - phrase(next_iter).start);
        phrase(next_iter).power = ...
            ((phrase(iter).power * phrase_length) + (phrase(next_iter).power * phrase_next_length)) ...
            / (phrase_length + phrase_next_length);
        phrase(next_iter).noise_power = ...
            ((phrase(iter).noise_power * phrase_length) + (phrase(next_iter).noise_power * phrase_next_length)) ...
            / (phrase_length + phrase_next_length);
        
        phrase_detection((min_index + 1) : (max_index - 1)) = phrase_detection(phrase(next_iter).start + 1);
        
        phrase(next_iter).start = min_index;
        phrase(next_iter).end = max_index;
        
        phrase(iter) = [];
    else
        iter = iter + 1;
    end
end


%% compute mean power of phrases
mean_phrase_power = 0;
mean_noise_power = 0;
for iter = 1 : length(phrase)
    mean_phrase_power = mean_phrase_power + phrase(iter).power;
    mean_noise_power = mean_noise_power + phrase(iter).noise_power;
end
mean_phrase_power = mean_phrase_power / length(phrase);
mean_noise_power = mean_noise_power / length(phrase);


%% union neibours phrases
phrase_power_threshold = threshold * mean_phrase_power;
noise_power_threshold = (1 - threshold) * (1 - mean_noise_power);
iter = 1;
while (iter <= length(phrase))
    if (phrase_power_threshold < phrase(iter).power)
        phrase(iter).voice = 1;
    elseif ((phrase_power_threshold < 5*phrase(iter).power) ...
            && (noise_power_threshold < (1 - phrase(iter).noise_power)))
        phrase(iter).voice = 1;
    else
        phrase(iter).voice = 0;
    end
    
    if (iter ~= 1)
        if (phrase(iter-1).voice == phrase(iter).voice)
            phrase(iter-1).end = phrase(iter).end;
            phrase_detection(phrase(iter-1).start+1 : phrase(iter-1).end-1) = phrase(iter-1).voice;
            phrase(iter) = [];
            continue;
        end
    end
    iter = iter + 1;
end


%%  compute real sample indexes
for iter = 1 : length(phrase)
    start_index = (phrase(iter).start - 1) * sample_size + 1;
    end_index = phrase(iter).end * sample_size;
    phrase(iter).start = start_index;
    phrase(iter).end = end_index;
end
phrase(1).start = 1;
phrase(end).end = length(x);


%% spread voice in time
spread_voice = round(50 * 10^(-3) * fs);
iter = 2;
while (iter < length(phrase))
    if (phrase(iter).voice)
        phrase(iter).start = phrase(iter).start - spread_voice;
        phrase(iter).end = phrase(iter).end + spread_voice;
    else
        phrase(iter).start = phrase(iter - 1).end + 1;
        phrase(iter).end = phrase(iter + 1).start - 1 - spread_voice;
    end
    
    phrase(iter).start = max(phrase(iter).start, 1);
    phrase(iter).end = min(phrase(iter).end, length(x));
    
    if (phrase(iter).start < phrase(iter).end)
        iter = iter + 1;
    else
        phrase(iter-1).end = phrase(iter).end;
        phrase(iter) = [];
    end
end
phrase(1).start = 1;
phrase(1).end = phrase(2).start - 1;
phrase(end).start = phrase(end - 1).end + 1;
phrase(end).end = length(x);


%% create output sample array
voice_length = 0;
noise_length = 0;
voice_power = 0;
noise_power = 0;
y = zeros(size(x));
noise = zeros(size(x));
phrase_space = zeros(size(x));


betta = 3;
false_snr = 5;
for iter = 1 : length(phrase)
    phrase_length = phrase(iter).start : phrase(iter).end;
    if (phrase(iter).voice)
        y(phrase_length) = x(phrase_length) .* kaiser(length(phrase_length), betta);
        phrase_space(phrase_length) = -1;
        voice_length = voice_length + length(phrase_length);
        voice_power = voice_power + sum(x(phrase_length) .^ 2);
        noise(phrase_length) = zeros(size(phrase_length));
    else
        y(phrase_length) = zeros(size(phrase_length));
        noise(phrase_length) = x(phrase_length) .* kaiser(length(phrase_length), betta*false_snr);
        noise_length = noise_length + length(phrase_length);
        noise_power = noise_power + sum(x(phrase_length) .^ 2);
    end
    phrase_space(phrase(iter).end) = 0;
end

voice_power = sqrt(voice_power / voice_length);
noise_power = sqrt(noise_power / noise_length);
if (noise_power ~= 0)
    real_snr = 10*log10(voice_power / noise_power);
else
    real_snr = 100;
end


% % %%  plotting VAD
% time_space = 0 : 1/fs : length(x)/fs - 1/(fs);
% figure
% subplot(4,1,1)
% plot(time_space, x)
% title('����������� ����������� ������')
% axis tight
% 
% reduce_fs = fs * length(power_array)/length(x);
% reduced_time_sample = 0 : 1/reduce_fs : time_space(end);
% subplot(4,1,2)
% % plot(phrase_detection .* mean(power_array), 'b');
% hold on;
% [ax, p1, p2] = plotyy(reduced_time_sample, power_array, reduced_time_sample, noise_power_array);
% ax(1).YAxis.Color = 'k';
% ax(2).YAxis.Color = 'k';
% noise_rgb = [27/255, 217/255, 39/255];
% ylabel(ax(1),'�������� �������','FontSize',15,'Color','r', 'FontWeight','bold')
% ylabel(ax(2), '�������� ����','FontSize',15,'Color',noise_rgb, 'FontWeight','bold');
% p1.Color = 'r';
% p2.Color = noise_rgb;
% p1.LineWidth = 1;
% p2.LineWidth = 1;
% % plot(reduced_time_sample, power_array, 'r', reduced_time_sample, noise_power_array, 'g', 'LineWidth', 2);
% % legend('�������� �������', '�������� ����')
% title('��������� �������������� �������')
% axis tight
% 
% subplot(4,1,3)
% plot(time_space, y)
% hold on;
% plot(time_space, noise, 'g');
% plot(time_space, phrase_space, 'k', 'LineWidth', 2);
% title('������������ �����������')
% axis tight


%%  deduction noise
band_start = 30;
band_stop = 5e3;
band_pass = [band_start, band_stop];
if (real_snr < 8)
    y = noise_suppression( y, noise, fs, band_stop );
end

%% delete zero data
for iter = length(phrase) : -1 : 1
    phrase_length = phrase(iter).start : phrase(iter).end;
    if (~phrase(iter).voice)
        y(phrase_length) = [];
    end
end

% % y(length(y) + 1 : length(x)) = 0;
% % figure
% subplot(4,1,4)
% % plot(time_space, y)
% plot(y)
% axis tight
% title('����������� �����������')
% % return
% %
% % subplot(2,1,2)
% % plot(noise, 'b');
% % axis tight
% 
% 
% % %% cut spectrum space
% % spectrum_space = 0 : fs/length(x) : (fs/2) - (1/length(x));
% % spectrum_space = spectrum_space(spectrum_space <= band_stop);
% %
% 

%% look spectrum

% index_space = log_index_space(amount_point, length(spectrum_space), decimation_speed);

% [~, log_space] = eject_spectr(voice_length, x, fs, band_pass, amount_point);

[density_spectrum_voice, log_space] = eject_spectr(voice_length, x, fs, band_pass);
[spectrum_filt_voice, ~] = eject_spectr(voice_length, y, fs, band_pass);
[spectrum_noise, ~] = eject_spectr(noise_length, noise, fs, band_pass);


%%  deduction noise
spectrum_noise = noise_deduction(spectrum_noise, threshold, false_snr);
spectrum_filt_voice = spectrum_filt_voice - spectrum_noise;

spectrum_voice_base = min(spectrum_filt_voice);
spectrum_filt_voice = max(spectrum_filt_voice - spectrum_voice_base, 0);

% 
% figure
% subplot(3,1,1)
% % semilogx(log_space, density_spectrum_voice, 'b')
% plot(log_space, density_spectrum_voice, 'b')
% axis tight
% subplot(3,1,2)
% % semilogx(log_space, spectrum_filt_voice, 'b')
% plot(log_space, spectrum_filt_voice, 'b')
% axis tight
% subplot(3,1,3)
% % semilogx(log_space, spectrum_noise, 'b')
% plot(log_space, spectrum_noise, 'b')
% axis tight

% sound(y, fs)

%% spetrogramm

% in seconds
buffer_duration = 0.1;

% in percent
overlap = 50;

sample_size = round(buffer_duration * fs);
buffer_size = round( ((1 + 2*(overlap / 100))*buffer_duration) * fs);

time_space = 0 : sample_size/fs : length(y)/fs - 1/(fs);
[~, log_space] = eject_spectr(buffer_size, x(1 : buffer_size), fs, band_pass);


% array of power buffer samples
db_range = 25;
spectrum_array = zeros(length(time_space), length(log_space));
for iter = 1 : length(time_space)
    
    start_index = (iter - 1) * sample_size;
    start_index = max(round(start_index - (overlap / 100)*sample_size), 1);
    end_index = min(start_index + buffer_size, length(y));
    
    phrase_space = start_index : end_index;
    
    buffer = y(phrase_space) ./ max(y(phrase_space));
    buffer = buffer .* kaiser(length(buffer), betta);
    
    spectr = eject_spectr(buffer_size, buffer, fs, band_pass);
    spectr = max(spectr - (max(spectr) - db_range), 0);
    spectrum_array(iter, 1 : length(log_space)) = spectr;
    
end

% spectrum_array = spectrum_array';

win_len = 11;
win = fspecial('gaussian',win_len,2);
win = win ./ max(win(:));
spectrum_array  = filter2(win, spectrum_array) ./ (win_len*2);

% figure
% mesh(spectrum_array)
% title('�������������')
% axis tight

pca_space = (1 : length(log_space));
std_spectr = std(spectrum_array, 0, 1);
mean_spectr = mean(spectrum_array, 1);
figure
plot(pca_space, mean_spectr, 'b', 'LineWidth', 3)
hold on
plot(pca_space, std_spectr, 'k', 'LineWidth', 3)
title('������� ������� ���������')
legend('������� �������� �������������', '��� �������������')
axis tight

amount_bar = 200;
[X, Y] = meshgrid(1:800, 0 : db_range / (amount_bar-1) : db_range);
[n, ~] = hist(spectrum_array, amount_bar);
[M, ~] = max(n);

for iter = 1 : 800
    n(:,iter) = n(:,iter) ./ M(iter);
end
n = filter2(win, n) ./ (2*win_len);


figure('Color', 'w');
s = mesh(X, Y, n);
s.FaceColor = 'k';
title('����������� ������������� ������������ ���������','FontSize',18)
xlabel('��������� ������������','FontSize',15)
ylabel('����������� ������������','FontSize',15)
% colormap winter
axis tight

% spectrum_array = spectrum_array';
% [n, ~] = hist(spectrum_array, 100);
% n = filter2(win, n) ./ (win_len);
% figure
% mesh(n)
% axis tight


% [X, Y] = meshgrid(log_space, time_space);
% [X, Y] = meshgrid(time_space, 1 : length(log_space));

% figure
% surf(X, Y, spectrum_array);
% axis tight



end

