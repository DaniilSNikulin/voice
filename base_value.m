function [ value ] = base_value( array )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

[nelem, centres] = hist(array, 100);
[~, index_max] = max(nelem);
value = centres(index_max);

end

