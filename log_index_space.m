function [ index_space ] = log_index_space( amount_point, input_length, decimation_speed )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


index_space = exp(((0 : (amount_point - 1)) ./ (amount_point - 1)) .* decimation_speed );
index_space = index_space - index_space(1);
index_space = index_space ./ index_space(end);
index_space = round((input_length - 1) .* index_space + 1);

end

